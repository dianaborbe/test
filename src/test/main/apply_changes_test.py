import importlib.util
import os
import sys


# Test kafka script
def test_kafka():
    env = "dev"
    app_name = "middleware-wallet"
    dest_repo = "../resources/dest_repo"
    source_repo = "../resources/source_repo"
    kafka_url = "kafka-hadoop.dev.knockout.local:2181"
    os.environ["KAFKA_TOPICS"] = "/home/local/KNOCKOUT/dianab/Downloads/kafka_2.12-1.1.0/bin/kafka-topics.sh"

    spec = importlib.util.spec_from_file_location("kafkalib", "../../python/main/backend_changes/kafka/apply_kafka_changes.py")
    kafka = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(kafka)

    kafka.apply_kafka(env, kafka_url, app_name, source_repo, dest_repo)

# Test bonus script
def test_bonus():
    env = "dev"
    app_name = "middleware-wallet"
    dest_repo = "../resources/dest_repo"
    source_repo = "../resources/source_repo"
    bonus_url = "http://docker-1.dev1.knockout.local:8081/v1/definitions"
    admin_user = "admintool"
    admin_password = "1admintool!"

    spec = importlib.util.spec_from_file_location("bonuslib", "../../python/main/backend_changes/bonus/apply_bonus_changes.py")
    bonus = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(bonus)

    bonus.apply_bonus(app_name, env, bonus_url, admin_user, admin_password, source_repo, dest_repo)


def test():
    env = "dev"
    app_name = "middleware-wallet"
    dest_repo = "../resources/dest_repo"
    source_repo = "../resources/source_repo"

    bonus_url = "http://docker-1.dev1.knockout.local:8081/v1/definitions"
    admin_user = "admintool"
    admin_password = "1admintool!"
    kafka_url = "kafka-hadoop.dev.knockout.local:2181"

    os.environ['PATH'] += os.pathsep + "/home/local/KNOCKOUT/dianab/Downloads/kafka_2.12-1.1.0/bin"
    spec = importlib.util.spec_from_file_location("changelib", "../../python/main/backend_changes/apply_backend_changes.py")
    changes = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(changes)

    #changes.apply_changes(app_name, env, source_repo, dest_repo, bonus_url, admin_user, admin_password, kafka_url)
    changes.apply_changes(app_name, env, source_repo, dest_repo)

#This function is the main one, entrypoint of the script
def main():
    test()

if __name__ == "__main__":
    main()
