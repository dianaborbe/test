import argparse
import json
import sys
import requests
from requests.auth import HTTPBasicAuth

# Prepare users to use them as input data
# - update organization id in the user details,
# - remove the old user id. At creation, the user will receive another id, otherwise Zendesk will try to update an user that doesn't exist and will fail
def prepareUsers(users, new_organization_id):
    for user in users:
        del user['id']
        user['organization_id'] = new_organization_id

    return users

# Prepare organizations data for migration
def prepareOrganizationsData(json_organizations):
    del json_organizations['next_page']
    del json_organizations['previous_page']
    del json_organizations['count']

    return json_organizations

# Print organization details
def printOrganizationDetails(organization):
    organization_id = organization['id']
    organization_name = organization['name']
    print ('*****Organization id: ', organization_id, ', name: ', organization_name, '.*****')
    
    return

# Get new organization id using the organization name. Organization name is unique.
def getNewOrganizationId(organization_name, url_base, target_auth):
    print ('*****Retrieving the new organization id for ', organization_name, '!*****')
    get_organization_by_name_url = url_base + '/organizations/autocomplete.json?name=' + organization_name

    response = None
    new_organization_id = None
    
    try:
        response = requests.get(get_organization_by_name_url, auth = target_auth, verify = True)

        if response.status_code != 200:
            print (response.status_code, response.content)
            print ('*****Something went wrong while getting the new organization id for: ', organization_name, '!*****')
            return None
        else: 
            response = json.loads(response.content)['organizations']
            for org in response:
                if org['name'] == organization_name:
                    new_organization_id = org['id']
                    break
    except Exception as inst:
        print (inst)
        return None
    
    return new_organization_id

# Perform request to create users
def createUsers(target_create_many_users_url, url_base, users_data, target_auth, organization_name):
    print ('*****Creating users on the target instance for ', organization_name, '.*****')
    users_creation_response = None
    
    try:
        users_creation_response = requests.post(target_create_many_users_url, json = users_data, auth = target_auth, verify = True)

        if users_creation_response.status_code == 200:
            # Check the status of the users creation
            task_status = json.loads(users_creation_response.content)["job_status"]["id"]
            status = requests.get(url_base + '/job_statuses/' + task_status + '.json', auth = target_auth, verify = True)
            print ('*****Checking users creation: ', status.status_code, '.*****')
        else:
            print (users_creation_response.status_code, users_creation_response.content)
            print ('*****Something went wrong while creating the users for the organization: ', organization_name, '!*****')
            return None
    except Exception as inst:
        print (inst)
        return None

    return users_creation_response.content

def createNewOrganizations(target_create_many_organizations_url, url_base, organizations_data, target_auth):
    print ('*****Creating organizations on the target instance!*****')
    create_organizations_response = None
    
    try:
        create_organizations_response = requests.post(target_create_many_organizations_url, json = organizations_data, auth = target_auth, verify = True)

        if create_organizations_response.status_code == 200:
            task_status = json.loads(create_organizations_response.content)["job_status"]["id"]
            # Check the status of the creation
            status = requests.get(url_base + '/job_statuses/' + task_status + '.json', auth = target_auth, verify = True)
            print ('*****Checking organizations creation status: ', status.status_code, '.*****')
        else:
            print (create_organizations_response.status_code, create_organizations_response.content)
            print('*****Something went wrong while creating organizations on target instance!*****')
            return None
    except Exception as inst:
        print (inst)
        return None

    return create_organizations_response.content

# Get one page of organizations from target(max: 100 organizations)
def getNewOrganizations(target_url, target_auth):
    print ('*****Retrieving organizations from the target instance!*****')
    new_organizations_response = None
    
    try:
        new_organizations_response = requests.get(target_url, auth = target_auth, verify = True)
        if new_organizations_response.status_code != 200:
            print (new_organizations_response.status_code, new_organizations_response.content)
            print ('*****Something went wrong while retrieving the organizations from target instance!*****')
            return None
    except Exception as inst:
        print (inst)
        return None

    return new_organizations_response.content

# Get one page of organizations from origin(max: 100 organizations)
def getOldOrganizations(origin_url, origin_auth):
    print ('*****Retrieving organizations from the origin instance!*****')
    old_organizations_reponse = None

    try:
        old_organizations_reponse = requests.get(origin_url, auth = origin_auth, verify = True)
        if old_organizations_reponse.status_code != 200:
            print (old_organizations_reponse.status_code, old_organizations_reponse.content)
            print ('*****Something went wrong while retrieving the organizations from the origin instance!*****')
            return None
    except Exception as inst:
        print (inst)
        return None

    return old_organizations_reponse.content

# Retrieve arguments from commandline
def setScriptArguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("origin_url", help="The Zendesk instance, origin of the data, ex: https://knockout1494422918.zendesk.com")
    parser.add_argument("origin_user", help="The admin user used to retrieve the data from origin, ex: user@knockoutgaming.com/token")
    parser.add_argument("origin_token", help="The API token used to retrieve data from origin, ex: 5u5u1lJ4z4s2LxQz9hPgoQzUDXiLCFhQtiKjjzET")
    parser.add_argument("target_url", help="The Zendesk instance, target of the data, ex: https://knockoutgaming1524041875.zendesk.com")
    parser.add_argument("target_user", help="The admin user used to add data in the target instance, ex: user@knockoutgaming.com/token")
    parser.add_argument("target_token", help="The API token used to add data in the target instance, ex: jnqDh6NXJooS4ozusjYHLajyS2PHluZqmbuwBZzO")
    return parser

# Perform request to retrieve one page of users by organization(max: 100 users)
def getUsersByOrganizations(get_users_by_organization_url, origin_auth, organization_id):
    users_response = None
    try:
        users_response = requests.get(get_users_by_organization_url + str(organization_id), auth = origin_auth, verify = True)
        if users_response.status_code != 200:
            print (users_response.status_code, users_response.content)
            print('*****Something went wrong while retrieving users for the organization: ', organization_id, '!*****')
            return None
    except Exception as inst:
        print (inst)
        return None
    
    return json.loads(users_response.content)

# Perform migration for one page from organization's users
def createUsersForOrganizationPerPage(organization, new_organizations_data, new_organization_id, users, create_many_users_url, url_base, origin_auth, target_auth):
    organization_name = organization['name']

    if new_organization_id is None:
        sys.exit('*****Aborting migration. Could not find the organization recently migrated on the target instance!*****')
    else:
        print ('*****The new organization id for ', organization_name, ' is: ', new_organization_id, '.*****')

    # If there are zero users in the organization, skip to next organization
    print ('*****Migrating ', len(users['users']), ' users for the organization', organization_name, '.*****')
    if len(users['users']) == 0:
        return
    
    # Prepare users for migration
    users['users'] = prepareUsers(users['users'], new_organization_id)
        
    # Create the users
    users_creation_response = createUsers(create_many_users_url, url_base, users, target_auth, organization['name'])
    if users_creation_response is None:
        sys.exit('*****Aborting migration. Could not create the users for organization: ', old_organization_id, ' ', organization_name, '!*****')
    
    return

def createUsersForOrganization(organization, new_organizations_data, get_users_by_organization_url, create_many_users_url, url_base, origin_auth, target_auth):
    users = None
    old_organization_id = organization['id']
    organization_name = organization['name']
    
    # Get the users by organization, first page
    users = getUsersByOrganizations(get_users_by_organization_url, origin_auth, old_organization_id)
    if users is None:
        sys.exit('*****Aborting migration. Could not retrieve the users for organization: ', old_organization_id, '!*****')
    new_organization_id = getNewOrganizationId(organization_name, url_base, target_auth)
    createUsersForOrganizationPerPage(organization, new_organizations_data, new_organization_id, users, create_many_users_url, url_base, origin_auth, target_auth)

    # Next pages, if they exist
    while users['next_page'] is not None:
        get_users_by_organization_url = users['next_page']
        users = getUsersByOrganizations(get_users_by_organization_url, origin_auth, old_organization_id)
        if users is None:
            sys.exit('*****Aborting migration. Could not retrieve the users for organization: ', old_organization_id, '!*****')
        new_organization_id = getNewOrganizationId(organization_name, url_base, target_auth)
        createUsersForOrganizationPerPage(organization, new_organizations_data, new_organization_id, users, create_many_users_url, url_base, origin_auth, target_auth)

    return

def performMigrationPerPage(origin_url_base, target_url_base, origin_auth, target_auth, api_version, old_organizations_reponse):
    origin_get_users_by_organization_url = origin_url_base + api_version + '/users.json?role[]=end-user&organization_id='

    target_url = target_url_base + api_version + '/organizations.json'
    target_create_many_organizations_url = target_url_base + api_version + '/organizations/create_many.json'
    target_create_many_users_url = target_url_base + api_version + '/users/create_or_update_many.json'
    
    # Prepare organizations for migration
    organizations_data = prepareOrganizationsData(json.loads(old_organizations_reponse))
    
    ## Create organizations in the target Zendesk instance
    new_organizations_response = createNewOrganizations(target_create_many_organizations_url, target_url_base + api_version, organizations_data, target_auth)
    if new_organizations_response is None:
        sys.exit('*****Aborting migration. Could not create the new organizations!*****')

    ## Get the organizations from target Zendesk instance
    new_organizations_response = getNewOrganizations(target_url, target_auth)
    if new_organizations_response is None:
        sys.exit('*****Aborting migration. Could not retrieve the new organizations!*****')
    
    new_organizations_data = prepareOrganizationsData(json.loads(new_organizations_response))

    ## For each organization recently created
    for org in organizations_data['organizations']:
        printOrganizationDetails(org)
        createUsersForOrganization(org, new_organizations_data, origin_get_users_by_organization_url, target_create_many_users_url, target_url_base + api_version, origin_auth, target_auth)

def main():
    args = setScriptArguments().parse_args()
    
    origin_url_base = args.origin_url
    origin_admin_user = args.origin_user
    origin_api_token = args.origin_token
    target_url_base = args.target_url
    target_admin_user = args.target_user
    target_api_token = args.target_token

    origin_auth = HTTPBasicAuth(origin_admin_user, origin_api_token)
    target_auth = HTTPBasicAuth(target_admin_user, target_api_token)

    print ('*****Starting data migration from ', origin_url_base, ' to ', target_url_base, '!*****')

    api_version = '/api/v2'
    origin_url = origin_url_base + api_version + '/organizations.json'

    ## Get the organizations to migrate from origin Zendesk instance, first page
    old_organizations_reponse = getOldOrganizations(origin_url, origin_auth)
    if old_organizations_reponse is None:
        sys.exit('*****Aborting migration. Could not retrieve the organizations!*****')
    performMigrationPerPage(origin_url_base, target_url_base, origin_auth, target_auth, api_version, old_organizations_reponse)

    ## Get the organizations to migrate from origin Zendesk instance, next pages
    while json.loads(old_organizations_reponse)['next_page'] is not None:
        origin_url = json.loads(old_organizations_reponse)['next_page']
        old_organizations_reponse = getOldOrganizations(origin_url, origin_auth)
        if old_organizations_reponse is None:
            sys.exit('*****Aborting migration. Could not retrieve the organizations!*****')
        performMigrationPerPage(origin_url_base, target_url_base, origin_auth, target_auth, api_version, old_organizations_reponse)

if __name__== "__main__":
  main()
