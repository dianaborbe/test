import argparse
import json
import sys
import requests
import subprocess
import os
from git import Repo


# This function commits to source repository the json flag files that track the execution of the scripts
# Args :
# - dest_repo - repository where to commit
# - app_name - the affected service, ex: middleware-wallet
# - type - the scripts type, ex: kafka
# - env - the affected environment, ex: dev
def commit_to_repository(dest_repo, app_name, type, env):
    os.chdir(dest_repo)
    repo = Repo('.', search_parent_directories=True)

    print("Untracked files:", str(repo.untracked_files))
    if len(repo.untracked_files) <= 0:
        sys.exit('Nothing to commit! Exiting script execution!')

    repo.remotes.origin.pull()
    repo.index.add(repo.untracked_files)

    message = "[" + env + "][" + type + "]" + "MNT - Add flag files for " + app_name
    repo.index.commit(message)
    print("Commit:", repo.head.commit)

    repo.remotes.origin.push()


# This function creates a kafka topic
# Args:
# - kafka_url - url to kafka server_url
# - kafka_path - path to kafka client
# - topic - topic description
def create_topic(kafka_url, kafka_path, topic):
    topic_name = topic['name']
    partitions = str(topic['partitions'])
    replicas = str(topic['replicas'])
    retention = str(topic['config']['retention.ms'])

    print("Creating topic:", topic_name)

    try:
        result = subprocess.check_output(
            [kafka_path, "--create",
             "--zookeeper", kafka_url,
             "--topic", topic_name,
             "--replication-factor", replicas,
             "--partitions", partitions,
             "--config", "retention.ms=", retention])
        return result
    except Exception as ex:
        print(ex)
        return "error"
        #sys.exit('Exiting script execution!')


# This function creates a kafka topic
# Args:
# - kafka_url - url to kafka server_url
# - kafka_path - path to kafka client
# - topic - topic description
def update_topic(kafka_url, kafka_path, topic):
    topic_name = str(topic['name'])
    retention = str(topic['config']['retention.ms'])

    print("Updating topic:", topic_name)

    try:
        result = subprocess.check_output(
            [kafka_path, "--alter",
             "--zookeeper", kafka_url,
             "--topic", topic_name,
             "--config", "retention.ms=", retention])
        return result
    except Exception as ex:
        print(ex)
        return "error"
        #sys.exit('Exiting script execution!')


# This function creates kafka topics
# Args:
# - source_repo - the file path where the bonus files are located
# - dest_repo - the path where you want to create the execution flag files
# - app_name - service affected, ex: middleware
# - type - type of the change, ex: kafka
# - env - environment affected, ex: dev
# - kafka_url - url to kafka server_url
# - kafka_path - path to kafka client
def execute_kafka(source_repo, dest_repo, app_name, type, env, kafka_url, kafka_path):
    source_type_path = source_repo + "/" + type  + "/"
    dest_app_type_env_path = dest_repo + "/" + app_name + "/" + type + "/" + env + "/"
    validate_repository(dest_app_type_env_path, create = True)

    # list the folder content
    for file in os.listdir(source_type_path):
        dest_file = (dest_app_type_env_path + file).rsplit(sep=".", maxsplit=1)[0] + ".json"

        # for all the json files that have not a flag yet
        if os.path.isfile(source_type_path + file) and file.endswith('.json') and not os.path.isfile(dest_file):
            try:
                # read the topic description
                with open(source_type_path + file) as f:
                    topic = json.load(f)

                #create/update the topic acconding the description
                if str(topic['type']) == "create":
                    response = create_topic(kafka_url, kafka_path, topic)
                elif str(topic['type']) == "update":
                    response = update_topic(kafka_url, kafka_path, topic)
                else:
                    response = None

                print("Response:", response)

                if response is "error" or response is None:
                    continue

                with open(dest_file, 'w') as outfile:
                    outfile.write(str(response))
            except subprocess.CalledProcessError as e:
                print(e)
                with open(dest_file, 'w') as outfile:
                    outfile.write(str(e))


# This function is validation a directory exists
# If the directory doesn't exist, it creates it
# Args:
# - path - the directory you want to validate
# - create - specify if you want to create the directory if it doesn't exist
def validate_repository(path, create):
    if os.path.isdir(path):
        print(path, " exists!")
    else:
        print(path, "doesn't exist!")
        if create:
            os.makedirs(path, exist_ok=True)
            print("Creating it!")
        else:
            sys.exit('Exiting script execution!')


# This function is validating if a file exists
# If the file doesn't exist, it exits the script execution
# Args:
# - file_path - the file you want to validate
def validate_file(file_path):
    if os.path.isfile(file_path):
        print(file_path, " exists!")
    else:
        print(file_path, "doesn't exist!")
        sys.exit('Exiting script execution!')


# This function executes the kafka scripts
# Args:
# - env - environment affected, ex: dev
# - kafka_url - url to kafka server_url
# - app_name - service affected, ex: middleware
# - source_repo - the file path where the bonus files are located
# - dest_repo - the path where you want to create the execution flag files
def apply_kafka(env, kafka_url, app_name, source_repo, dest_repo):
    print("*****Starting script to apply kafka changes.*****")

    validate_repository(source_repo, create = False)
    validate_repository(dest_repo, create = True)

    type = "kafka"
    kafka_path = "kafka-topics.sh"
    validate_file(kafka_path)

    execute_kafka(source_repo, dest_repo, app_name, type, env, kafka_url, kafka_path)
    commit_to_repository(dest_repo, app_name, type, env)
    
    print("*****Script end!*****")


# This function retrieves the arguments from commandline
def setScriptArguments():
    parser = argparse.ArgumentParser()

    parser.add_argument("app_name",
        help="The application name, ex: middleware-wallet")
    parser.add_argument("env", help="The environment, ex: dev")
    parser.add_argument("server_url", help="The kafka server url")

    parser.add_argument("source_repo", help="The source repository")
    parser.add_argument("dest_repo", help="The destination repository")

    return parser


# This function is the main one, entrypoint of the script
def main():
    args = setScriptArguments().parse_args()
    env = args.env
    server_url = args.server_url
    app_name = args.app_name
    source_repo = args.source_repo
    dest_repo = args.dest_repo

    apply_kafka(env, app_name, source_repo, dest_repo)


if __name__ == "__main__":
    main()
