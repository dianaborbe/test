# Bonus

## Prerequisites:

 - Git: 2.7.2
 - Python: 3.5.2 (+ requests, gitpython)
 - Kafka client: 2.12-1.1.0


## Usage:

- Clone this repository
- Go to directory *src/python/backend_changes*
- Execute: *python3 apply_changes.py env app_name source_repo dest_repo*
