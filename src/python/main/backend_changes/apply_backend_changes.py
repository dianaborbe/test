import argparse
import json
import sys
import requests
import subprocess
import os
from git import Repo
from requests.auth import HTTPBasicAuth
import configparser


# This function commits to source repository the json flag files that track the execution of the scripts
# Args :
# - dest_repo - repository where to commit
# - app_name - the affected service, ex: middleware-wallet
# - type - the scripts type, ex: bonus
# - env - the affected environment, ex: dev
def commit_to_repository(dest_repo, app_name, type, env):
    repo = Repo(dest_repo, search_parent_directories=True)

    print("Untracked files:", str(repo.untracked_files))
    if len(repo.untracked_files) <= 0:
        print('Nothing to commit for:', type)
        return

    repo.remotes.origin.pull()
    repo.index.add(repo.untracked_files)

    os.environ['GIT_USERNAME'] = "Jenkins"

    message = "[" + env + "][" + type + "]" + "MNT - Add flag files for " + app_name
    repo.index.commit(message)
    print("Commit:", repo.head.commit)

    repo.remotes.origin.push()


# This function executes a POST request to add bonus from file
# Args:
# - file - the file path which contains the bonus we want to adduser
# - bonus_url - url to use to perform the POST request
# - bonus_auth - authentication to api in order to be able to perform the POST request
def execute_bonus_for_file(file, bonus_url, bonus_auth):
    with open(file) as f:
        data = json.load(f)

    try:
        bonus_response = requests.post(
            bonus_url, json=data, auth=bonus_auth, verify=True)

        return bonus_response
    except Exception as ex:
        print (ex)
        sys.exit('Exiting script execution!')


# This function adds bonuses from a specific path
# Args:
# - source_repo - the file path where the bonus files are located
# - dest_repo - the path where you want to create the execution flag files
# - url - the url used to execute the POST request
# - auth - authentication to api in order to be able to perform the POST request
# - app_name - service affected, ex: middleware
# - type - the type of the change, ex: bonus
# - env - environment affected, ex: dev
def execute_bonus(source_repo, dest_repo, url, auth, app_name, type, env):

    source_type_path = source_repo + "/" + type + "/"
    dest_app_type_env_path = dest_repo + "/" + app_name + "/" + type + "/" + env + "/"
    validate_repository(dest_app_type_env_path, create = True)

    # list the folder content
    for file in os.listdir(source_type_path):
        dest_file = dest_app_type_env_path + file

        # for all the json files that have not a flag yet
        if os.path.isfile(source_type_path + file) and file.endswith('.json') and not os.path.isfile(dest_file):
            # execute the POST request
            response = execute_bonus_for_file(
                source_type_path + file, url, auth)

            print("Add bonus from file:", file, ". Response:", str(response.json()))

            # if the response is not successful, go to next file
            if response.status_code in (200, 201, 204):
                continue
                #sys.exit('Exiting script execution!')?

            # write response
            with open(dest_file, 'w') as outfile:
                outfile.write(str(response.json()))


# This function creates a kafka topic
# Args:
# - kafka_url - url to kafka server_url
# - kafka_path - path to kafka client
# - topic - topic description
def create_topic(kafka_url, kafka_path, topic):
    topic_name = topic['name']
    partitions = str(topic['partitions'])
    replicas = str(topic['replicas'])
    retention = str(topic['config']['retention.ms'])

    print("Creating topic:", topic_name)
    try:
        result = subprocess.check_output(
            [kafka_path, "--create",
             "--zookeeper", kafka_url,
             "--topic", topic_name,
             "--replication-factor", replicas,
             "--partitions", partitions,
             "--config", "retention.ms=", retention])
        return result
    except Exception as ex:
        print(ex)
        return "error"
        #sys.exit('Exiting script execution!')


# This function creates a kafka topic
# Args:
# - kafka_url - url to kafka server_url
# - kafka_path - path to kafka client
# - topic - topic description
def update_topic(kafka_url, kafka_path, topic):
    topic_name = str(topic['name'])
    retention = str(topic['config']['retention.ms'])

    print("Updating topic:", topic_name)
    try:
        result = subprocess.check_output(
            [kafka_path, "--alter",
             "--zookeeper", kafka_url,
             "--topic", topic_name,
             "--config", "retention.ms=", retention])
        return result
    except Exception as ex:
        print(ex)
        return "error"
        #sys.exit('Exiting script execution!')


# This function creates kafka topics
# Args:
# - source_repo - the file path where the bonus files are located
# - dest_repo - the path where you want to create the execution flag files
# - app_name - service affected, ex: middleware
# - type - type of the change, ex: kafka
# - env - environment affected, ex: dev
# - kafka_url - url to kafka server_url
# - kafka_path - path to kafka clientos.environ['GIT_USERNAME']
def execute_kafka(source_repo, dest_repo, app_name, type, env, kafka_url, kafka_path):
    source_type_path = source_repo + "/" + type  + "/"
    dest_app_type_env_path = dest_repo + "/" + app_name + "/" + type + "/" + env + "/"
    validate_repository(dest_app_type_env_path, create = True)

    # list the folder content
    for file in os.listdir(source_type_path):
        dest_file = (dest_app_type_env_path + file).rsplit(sep=".", maxsplit=1)[0] + ".json"

        # for all the json files that have not a flag yet
        if os.path.isfile(source_type_path + file) and file.endswith('.json') and not os.path.isfile(dest_file):
            try:
                # read the topic description
                with open(source_type_path + file) as f:
                    topic = json.load(f)

                #create/update the topic acconding the description
                if str(topic['type']) == "create":
                    response = create_topic(kafka_url, kafka_path, topic)
                elif str(topic['type']) == "update":
                    response = update_topic(kafka_url, kafka_path, topic)
                else:
                    response = None

                print("Response:", response)

                if response is "error" or response is None:
                    continue

                with open(dest_file, 'w') as outfile:
                    outfile.write(str(response))
            except subprocess.CalledProcessError as e:
                print(e)
                with open(dest_file, 'w') as outfile:
                    outfile.write(str(e))


# This function is validation a directory exists
# If the directory doesn't exist, it creates it
# Args:
# - path - the directory you want to validate
def validate_repository(path, create):
    if os.path.isdir(path):
        print(path, " exists!")
        return True

    print(path, "doesn't exist!")
    if create:
        os.makedirs(path, exist_ok=True)
        print("Creating it!")
        return True

    return False


# This function is validating if a file exists
# If the file doesn't exist, it exits the script execution
# Args:
# - file_path - the file you want to validate
def validate_file(file_path):
    if os.path.isfile(file_path):
        print(file_path, " exists!")
    else:
        print(file_path, "doesn't exist!")
        sys.exit('Exiting script execution!')


# This function executes the bonus scripts
# Args:
# - env - environment affected, ex: dev
# - bonus_url - url to kafka server_url
# - admin_user - username to access api
# - admin_password - password to access api
# - app_name - service affected, ex: middleware
# - source_repo - the file path where the bonus files are located
# - dest_repo - the path where you want to create the execution flag files
def apply_bonus(env, bonus_url, admin_user, admin_password, app_name, source_repo, dest_repo):
    validate_repository(dest_repo, create = True)
    print("*****Starting script to apply bonus changes.*****")

    bonus_auth = HTTPBasicAuth(admin_user, admin_password)
    type = "bonus"

    execute_bonus(source_repo, dest_repo, bonus_url, bonus_auth, app_name, type, env)
    commit_to_repository(dest_repo, app_name, type, env)

    print("*****Script end!*****")


# This function executes the kafka scripts
# Args:
# - env - environment affected, ex: dev
# - kafka_url - url to kafka server_url
# - app_name - service affected, ex: middleware
# - source_repo - the file path where the bonus files are located
# - dest_repo - the path where you want to create the execution flag files
def apply_kafka(env, kafka_url, app_name, source_repo, dest_repo):
    validate_repository(dest_repo, create = True)
    print("*****Starting script to apply kafka changes.*****")

    type = "kafka"
    kafka_path = "kafka-topics.sh"

    execute_kafka(source_repo, dest_repo, app_name, type, env, kafka_url, kafka_path)
    commit_to_repository(dest_repo, app_name, type, env)

    print("*****Script end!*****")


def apply_changes(app_name, env, source_repo, dest_repo, bonus_url, admin_user, admin_password, kafka_url):

    if validate_repository(source_repo + "/bonus", create = False) is True:
        apply_bonus(env, bonus_url, admin_user, admin_password, app_name, source_repo, dest_repo)

    if validate_repository(source_repo + "/kafka", create = False) is True:
        apply_kafka(env, kafka_url, app_name, source_repo, dest_repo)


# This function retrieves the arguments from commandline
def setScriptArguments():
    parser = argparse.ArgumentParser()

    parser.add_argument("app_name",
        help="The application name, ex: middleware-wallet")
    parser.add_argument("env", help="The environment, ex: dev")
    parser.add_argument("source_repo", help="The source repository")
    parser.add_argument("dest_repo", help="The destination repository")

    parser.add_argument("--bonus_url", help="The api url for bonus. If not speficied, it will take it from BONUS_URL environment variable.")
    parser.add_argument("--admin_user", help="The admin user of the api. If not speficied, it will take it from ADMIN_USER environment variable.")
    parser.add_argument("--admin_password", help="The password of the admin user. If not speficied, it will take it from ADMIN_PASSWORD environment variable.")

    parser.add_argument("--kafka_url", help="The kafka server url. If not speficied, it will take it from KAFKA_URL environment variable.")

    return parser


# This function is the main one, entrypoint of the script
def main():
    args = setScriptArguments().parse_args()

    app_name = args.app_name
    env = args.env
    source_repo = args.source_repo
    dest_repo = args.dest_repo

    os.environ["BONUS_URL"] = "1"
    os.environ["ADMIN_USER"] = "1"
    os.environ["ADMIN_PASSWORD"] = "1"
    os.environ["KAFKA_URL"] = "1"

    bonus_url = args.bonus_url
    if bonus_url is None:
        bonus_url = os.environ['BONUS_URL']
        print("Using bonus_url=", bonus_url)

    admin_user = args.admin_user
    if admin_user is None:
        admin_user = os.environ['ADMIN_USER']
        print("Using admin_user=", admin_user)

    admin_password = args.admin_password
    if admin_password is None:
        admin_password = os.environ['ADMIN_PASSWORD']
        print("Using admin_password=", admin_password)

    kafka_url = args.kafka_url
    if kafka_url is None:
        kafka_url = os.environ['KAFKA_URL']
        print("Using kafka_url=", kafka_url)

    apply_changes(app_name, env, source_repo, dest_repo, bonus_url, admin_user, admin_password, kafka_url)

if __name__ == "__main__":
    main()
