# Bonus

## Prerequisites:

 - Python: 3.5.2 (+ requests, gitpython)
 - Git: 2.7.2


## Usage:

- Clone this repository
- Go to directory *src/python/backend_changes/kafka*
- Execute: *python3 apply_kafka_changes.py app_name env url admin_user admin_password source_repo dest_repo*
