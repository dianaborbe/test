import argparse
import json
import sys
import requests
import subprocess
import os
from git import Repo
from requests.auth import HTTPBasicAuth
import configparser

# This function commits to source repository the json flag files that track the execution of the scripts
# Args :
# - dest_repo - repository where to commit
# - app_name - the affected service, ex: middleware-wallet
# - type - the scripts type, ex: bonus
# - env - the affected environment, ex: dev
def commit_to_repository(dest_repo, app_name, type, env):
    os.chdir(dest_repo)
    repo = Repo('.', search_parent_directories=True)

    print("Untracked files:", str(repo.untracked_files))
    if len(repo.untracked_files) <= 0:
        sys.exit('Nothing to commit! Exiting script execution!')

    repo.remotes.origin.pull()
    repo.index.add(repo.untracked_files)

    message = "[" + env + "][" + type + "]" + "MNT - Add flag files for " + app_name
    repo.index.commit(message)
    print("Commit:", repo.head.commit)

    repo.remotes.origin.push()


# This function executes a POST request to add bonus from file
# Args:
# - file - the file path which contains the bonus we want to adduser
# - bonus_url - url to use to perform the POST request
# - bonus_auth - authentication to api in order to be able to perform the POST request
def execute_bonus_for_file(file, bonus_url, bonus_auth):
    with open(file) as f:
        data = json.load(f)

    try:
        bonus_response = requests.post(
            bonus_url, json=data, auth=bonus_auth, verify=True)

        return bonus_response
    except Exception as ex:
        print (ex)
        sys.exit('Exiting script execution!')


# This function adds bonuses from a specific path
# Args:
# - source_repo - the file path where the bonus files are located
# - dest_repo - the path where you want to create the execution flag files
# - url - the url used to execute the POST request
# - auth - authentication to api in order to be able to perform the POST request
# - app_name - service affected, ex: middleware
# - type - the type of the change, ex: bonus
# - env - environment affected, ex: dev
def execute_bonus(source_repo, dest_repo, url, auth, app_name, type, env):

    source_type_path = source_repo + "/" + type + "/"
    dest_app_type_env_path = dest_repo + "/" + app_name + "/" + type + "/" + env + "/"
    validate_repository(dest_app_type_env_path, create = True)

    # list the folder content
    for file in os.listdir(source_type_path):
        dest_file = dest_app_type_env_path + file

        # for all the json files that have not a flag yet
        if os.path.isfile(source_type_path + file) and file.endswith('.json') and not os.path.isfile(dest_file):
            # execute the POST request
            response = execute_bonus_for_file(
                source_type_path + file, url, auth)

            print("Add bonus from file:", file, ". Response:", str(response.json()))

            # if the response is not successful, go to next file
            if response.status_code not in (200, 201, 204):
                continue
                #sys.exit('Exiting script execution!')?

            # write response
            with open(dest_file, 'w') as outfile:
                outfile.write(str(response.json()))


# This function is validation a directory exists
# If the directory doesn't exist, it creates it
# Args:
# - path - the directory you want to validate
def validate_repository(path, create):
    if os.path.isdir(path):
        print(path, " exists!")
    else:
        print(path, "doesn't exist!")
        if create:
            os.makedirs(path, exist_ok=True)
            print("Creating it!")
        else:
            sys.exit('Exiting script execution!')


# This function is validating if a file exists
# If the file doesn't exist, it exits the script execution
# Args:
# - file_path - the file you want to validate
def validate_file(file_path):
    if os.path.isfile(file_path):
        print(file_path, " exists!")
    else:
        print(file_path, "doesn't exist!")
        sys.exit('Exiting script execution!')


# This function executes the bonus scripts
def apply_bonus(env, bonus_url, admin_user, admin_password, app_name, source_repo, dest_repo):
    print("*****Starting script to apply bonus changes.*****")

    validate_repository(source_repo, create = False)
    validate_repository(dest_repo, create = True)

    bonus_auth = HTTPBasicAuth(admin_user, admin_password)
    type = "bonus"

    execute_bonus(source_repo, dest_repo, bonus_url, bonus_auth, app_name, type, env)
    commit_to_repository(dest_repo, app_name, type, env)

    print("*****Script end!*****")


# This function retrieves the arguments from commandline
def setScriptArguments():
    parser = argparse.ArgumentParser()

    parser.add_argument("app_name",
        help="The application name, ex: middleware-wallet")
    parser.add_argument("env", help="The environment, ex: dev")
    parser.add_argument("url", help="The api url")

    parser.add_argument("admin_user", help="The admin user of the api")
    parser.add_argument("password", help="The password of the admin user")

    parser.add_argument("source_repo", help="The source repository")
    parser.add_argument("dest_repo", help="The destination repository")
    
    return parser


# This function is the main one, entrypoint of the script
def main():
    args = setScriptArguments().parse_args()
    env = args.env
    url = args.url
    admin_user = args.admin_user
    password = args.password
    app_name = args.app_name
    source_repo = args.source_repo
    dest_repo = args.dest_repo

    apply_bonus(env, url, admin_user, password, app_name, source_repo, dest_repo)

if __name__ == "__main__":
    main()
