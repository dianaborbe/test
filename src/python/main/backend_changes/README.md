# Bonus

## Prerequisites:

 - Git: 2.7.2
 - Python: 3.5.2 (+ requests, gitpython)
 - Kafka client: 2.12-1.1.0

 ## Usage:

 - Clone this repository
 - Go to directory *src/python/backend_changes*
 - Execute: *python3 apply_backend_changes.py app_name env source_repo dest_repo bonus_url admin_user admin_password kafka_url*
